#!/bin/bash

LOG_FILE=index.html
WEB_DIR=www
CLIENT_PID=pid.txt
CLIENT_PAGE=index.html
API_DIR_BASE=api_server
API_DIR=api_server/api
CLIENT_DIR=api_server/client
IP_FILE=ip.txt
PYT_LOG=~/www/python.html
GIT_URL=https://gitlab.com/baloghbence0915/depl_api_server.git

#Method for logging
log_state () {
        echo $(date +%Y-%m-%d" "%H:%M:%S.%4N) : "$1" "<br>" >> ~/$WEB_DIR/$LOG_FILE
}

#Method for logging from file
log_state_from_file(){
        while read p; do
                log_state "$p"
        done <$1
}

#Looking for unexpected states
check_success(){
        exit_code=$1
        if [ "$exit_code" -ne 0 ]
        then
				log_state '***********************'
				log_state '*An error has occured *'
				log_state '***********************'
				log_state "Exit code: $exit_code"
				exit 1
        fi
}

#Make file for host
cd ~
mkdir -p $WEB_DIR
cd  $WEB_DIR
curl https://ipinfo.io/ip > $IP_FILE
touch $LOG_FILE
echo "" > $LOG_FILE

#Start hosting, if can't force program to terminate
log_state "Start client server"
python3 -m http.server 8000 &> /dev/null &
PID=$!
sleep 1
LINES=$(ps -A | grep $PID | wc -l | awk '{ print $1}')
if [ "$LINES" -eq 0 ]
    then
		check_success 1
fi
log_state "Server started successful"
cd ..


#Save clients PID, for later purpose
touch $CLIENT_PID
echo $PID > $CLIENT_PID

log_state "Python PID:"
log_state $PID
log_state "Script started.."

#Updtate packages
log_state "Update packages.."
sudo apt-get update -y &> update.log
check_success $?
log_state_from_file update.log
log_state "Update finished."

#Install dependecies
log_state "Install apt-transport-https, ca-certificates, curl, gnupg-agent, software-properties-common.."
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common &> install.log
check_success $?
log_state_from_file install.log
log_state "Install finished."

#Set key
log_state "Set GPG key.."
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - &> key.log
check_success $?
log_state_from_file key.log
log_state "Set key successful."


#Set docker repository
log_state "Set Docker repisitory.."
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" &> repo.log
check_success $?
log_state_from_file repo.log
log_state "Set repository successful."


#Update packages again, including docker
log_state "Update packages.."
sudo apt-get update -y &> update.log
check_success $?
log_state_from_file update.log
log_state "Update finished."


#Install docker and cli
log_state "Install docker-ce, docker-ce-cli, containerd.io .."
sudo apt-get install -y docker-ce docker-ce-cli containerd.io &> install.log
check_success $?
log_state_from_file install.log
log_state "Install finished."


#Install git
log_state "Install git.."
sudo apt-get install -y git-core &> install.log
check_success $?
log_state_from_file install.log
log_state "Install finished."


#Clone git repo if not exits
log_state "Cloning GIT repo.."
if [ -d "$API_DIR_BASE" ] ; then
	log_state "Delete existing dir"
	rm -rf $API_DIR_BASE
fi
git clone "$GIT_URL" "$API_DIR_BASE" &> git.log
check_success $?
log_state_from_file git.log
log_state "Cloned successfully.."



if ! [ -x "$(command -v python3.7)" ]; then

	#Get python3.7
	log_state "INSTALL PYTHON 3.7"
	#Updtate packages
	log_state "Update packages.."
	sudo apt update -y &> update.log
	check_success $?
	log_state_from_file update.log
	log_state "Update finished."

	#Install packages for python
	log_state "Install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget.."
	sudo apt install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget &> install.log
	check_success $?
	log_state_from_file install.log
	log_state "Install finished."


	cd /tmp
	#Download python
	log_state "Download python.tar"
	wget https://www.python.org/ftp/python/3.7.2/Python-3.7.2.tar.xz
	check_success $?
	log_state "Download finished."


	log_state "Unzip python."
	tar -xf Python-3.7.2.tar.xz
	check_success $?
	log_state "Unzip finished."
	cd Python-3.7.2
	log_state "Configure pyhton."
	touch $PYT_LOG
	echo "*************************CONFIGURE*************************" >> $PYT_LOG
	./configure --enable-optimizations &>> $PYT_LOG
	check_success $?
	log_state "Configure Finished."	
	echo "*******************CONFIGURE***FINISHED********************" >> $PYT_LOG
	
	log_state "It's time to install pyhton, it can be take 10-20 minute"
	log_state "It can be follow <a href='python.html'>HERE</a>"
	echo "*************************BULD*************************" >> $PYT_LOG
	make -j 1 &>> $PYT_LOG
	check_success $?
	echo "********************BUILD***FINISHED******************" >> $PYT_LOG
	echo "*************************INSTALL*************************" >> $PYT_LOG
	sudo make altinstall &>> $PYT_LOG
	check_success $?
	echo "********************INSTALL***FINISHED*******************" >> $PYT_LOG
	echo "*****************<CLICK**BACK*ON*BROWSER>****************" >> $PYT_LOG
	
	if ! [ -x "$(command -v python3.7)" ]; then	
		log_state "Unable to install python3.7"
		check_success 1
	fi
	
	log_state "Python installed" 
	
else
	log_state "Python3.7 was found"
fi

if ! [ -x "$(command -v pip3.7)" ]; then	
	log_state "Not found pip3.7"
	check_success 1
else
	log_state "Pip3.7 was found"
fi

log_state "Start clients GUI"
cd ~

#Copy client files to http directory
TEMP=$LOG_FILE
LOG_FILE=log.html
mv $WEB_DIR/$TEMP $WEB_DIR/$LOG_FILE &> mv.log
log_state_from_file mv.log
cp $CLIENT_DIR/* $WEB_DIR &> cp.log
log_state_from_file cp.log


log_state "Install websockets for python"
sudo pip3.7 install websockets &> pip.log
check_success $?
log_state_from_file pip.log


log_state "Start API Server"

python3.7 ~/api_server/api/server.py &> /dev/null &
PID=$!
sleep 1
LINES=$(ps -A | grep $PID | wc -l | awk '{ print $1}')
if [ "$LINES" -eq 0 ]
    then
		check_success 1
fi
log_state "API Server started successful"

log_state "Exit code: 0"

exit 0