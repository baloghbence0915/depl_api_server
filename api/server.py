#!/usr/bin/env python

import asyncio
import json
import logging
import websockets
import random
import subprocess
import re
import sys


logging.basicConfig()

CONTAINERS = []

CLIENTS = set()


def addContainer(definition):
    id = "{}".format(random.randint(10000000,99999999))
    cmd = "sudo docker run --name {} {}".format(id,definition)
    success, stdout, stderr = run_cmd(cmd)
    if success == False:
        return False,json.dumps({'type': 'notify', 'success':False,'message':'An exception occured during execute command'});
    CONTAINERS.append({"id":id,"name":cmd,"stdout":stdout,"stderr":stderr})
    return True,json.dumps({'type': 'notify', 'success':True,'message':'Container creation was successfull'});

def invalidSintaxMessage():
    return json.dumps({'type': 'notify', 'success':False,'message':'You used invalid sintax, can\'t use & | < > # carachters!'});

def removeContainer(id):
    success, stdout, stderr = run_cmd("sudo docker rm -f {}".format(id))
    if success == False:
        return False,json.dumps({'type': 'notify', 'success':False,'message':'An exception occured during execute command'});
    CONTAINERS[:] = [d for d in CONTAINERS if d.get('id') != id]
    return True,json.dumps({'type': 'notify', 'success':True,'message':'Container destroy was successfull'});		


def containersToJSON():
    return json.dumps({'type': 'containers', 'containers':CONTAINERS})

	
def run_cmd(cmd):
    try:
        p = subprocess.run(cmd.split(),stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout = p.stdout.decode('ascii')
        stderr = p.stderr.decode('ascii')
        return True,stdout,stderr;
    except:
        pass
    return False,"","";

	
	
async def sendOutContainers():
    if CLIENTS:       # asyncio.wait doesn't accept an empty list
        message = containersToJSON()
        await asyncio.wait([user.send(message) for user in CLIENTS])

async def sendOutNotification(notification):
    if CLIENTS:       # asyncio.wait doesn't accept an empty list
        await asyncio.wait([user.send(notification) for user in CLIENTS])		


async def register(websocket):
    CLIENTS.add(websocket)


async def unregister(websocket):
    CLIENTS.remove(websocket)

def terminate():
    cmd = "sudo killall python3"
    try:
        file  = open("pid.txt", "r")
        pid = file.readline().split()[0]
        cmd = "sudo kill "+pid
    except:
        pass
    run_cmd(cmd)
    sys.exit()

async def receiveConnection(websocket, path):
    #save client
    await register(websocket)
    try:#send actual containers to client
        await websocket.send(containersToJSON())
        #read new messages from socket
        async for message in websocket:
            data = json.loads(message)
            c = data['content']
            a = data['action']
            #Fillter injection
            match = re.findall("[&|<>#]+", c)
            if( len(match) > 0):
                await sendOutNotification(invalidSintaxMessage())
                continue
            if a == 'add_container':
                success,notification = addContainer(c);
                if success:
                    await sendOutNotification(notification)
                    await sendOutContainers()
                else:
                    await sendOutNotification(notification)
            elif a == 'remove_container':
                success,notification = removeContainer(c);
                if success:
                    await sendOutNotification(notification)
                    await sendOutContainers()
                else:
                    await sendOutNotification(notification)
            elif a == 'exit':
                terminate()
            else:
                logging.error(
                    "unsupported event: {}", data)
    finally:
        await unregister(websocket)

asyncio.get_event_loop().run_until_complete(
    websockets.serve(receiveConnection, '0.0.0.0', 6789))
asyncio.get_event_loop().run_forever()