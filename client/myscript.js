var websocket;

$.get('ip.txt', function (ip) {
	websocket = new WebSocket("ws://" + ip + ":6789/");

	websocket.onmessage = function (event) {
		data = JSON.parse(event.data);
		switch (data.type) {
			case 'containers':
				refresh_data(data.containers);
				break;
			case 'notify':
				set_notification(data);
				break;
			default:
				console.error(
					"unsupported event", data);
		}
	};
});

$(document).ready(function () {
	$("#add").on('click', () => {
		if ($('#cmd').val() != "") {
			add_container($('#cmd').val())
		} else {
			alert("Can't add container without command");
		}
	});

	$('#notif').slideUp("slow");
	$('#notif').on('click', function () {
		$(this).slideUp("slow");
	});

	$('#closebutton').on('click', function () {
		close_connection();
	});
});

function close_connection() {
	websocket.send(JSON.stringify({ action: 'exit', content: '' }));

}

function set_notification(data) {
	$('#notif').html(data.message);

	$('#notif').removeClass();
	if (data.success) {
		$('#notif').addClass("notifsucc");
	} else {
		$('#notif').addClass("notiferr");
	}

	$('#notif').slideDown("slow");

}

function refresh_data(containers) {
	//prepare container DIV
	$('#containers').empty();

	for (let i = 0; i < containers.length; i++) {
		let c = containers[i];
		//Add header
		$('<div id="' + c.id + '" class="flip"></div>')
			.html(c.name + " id:" + c.id + " " + "(Click here for details)")
			.on("click", function () {
				//Toogle details section
				let id = c.id + "_details";
				$('#' + id).slideToggle("slow");
			})
			.appendTo("#containers");

		//Add button to header
		$("<button class='delete'>Delete</button>")
			.on("click", (e) => {
				e.stopPropagation();
				delete_container(c);
			}).appendTo('#' + c.id);

		//Add slide section
		$('<div id="' + c.id + '_details" class="panel"></div>')
			.append('<div class="stdout">' + c.stdout + '</div>')
			.append('<div class="stderr">' + c.stderr + '</div>')
			.appendTo("#containers");

	}
}

function delete_container(c) {
	websocket.send(JSON.stringify({ action: 'remove_container', content: "" + c.id }));
}

function add_container(cmd) {
	websocket.send(JSON.stringify({ action: 'add_container', content: cmd }));
}


